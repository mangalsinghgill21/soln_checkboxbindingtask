﻿CREATE TABLE [dbo].[tblcity] (
    [cityid]   NUMERIC (18)  NOT NULL,
    [cityname] NVARCHAR (50) NOT NULL,
    [stateid]  NUMERIC (18)  NOT NULL,
    CONSTRAINT [PK_tblcity] PRIMARY KEY CLUSTERED ([cityid] ASC)
);

