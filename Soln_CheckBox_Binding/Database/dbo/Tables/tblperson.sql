﻿CREATE TABLE [dbo].[tblperson] (
    [PersonId]  NUMERIC (18)   IDENTITY (1, 1) NOT NULL,
    [Firstname] NVARCHAR (MAX) NOT NULL,
    [Lastname]  NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_tblperson] PRIMARY KEY CLUSTERED ([PersonId] ASC)
);

