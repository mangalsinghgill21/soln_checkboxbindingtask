﻿CREATE TABLE [dbo].[tblcountry] (
    [countryid]   NUMERIC (18)  NOT NULL,
    [countryname] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_tblcountry] PRIMARY KEY CLUSTERED ([countryid] ASC)
);

