﻿CREATE TABLE [dbo].[tbluser] (
    [Userid]    NUMERIC (18)   IDENTITY (1, 1) NOT NULL,
    [Firstname] NVARCHAR (MAX) NOT NULL,
    [Lastname]  NVARCHAR (MAX) NOT NULL,
    [Onsite]    BIT            NOT NULL,
    CONSTRAINT [PK_tbluser] PRIMARY KEY CLUSTERED ([Userid] ASC)
);

