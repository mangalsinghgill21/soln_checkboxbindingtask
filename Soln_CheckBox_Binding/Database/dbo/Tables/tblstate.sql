﻿CREATE TABLE [dbo].[tblstate] (
    [stateid]   NUMERIC (18)  NOT NULL,
    [statename] NVARCHAR (50) NOT NULL,
    [countryid] NUMERIC (18)  NOT NULL,
    CONSTRAINT [PK_tblstate] PRIMARY KEY CLUSTERED ([stateid] ASC)
);

