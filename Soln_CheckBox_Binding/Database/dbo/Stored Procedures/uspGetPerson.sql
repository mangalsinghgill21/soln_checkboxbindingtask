﻿CREATE PROCEDURE [dbo].[uspGetPerson]
	@Command Varchar(100)=NULL,

	@PersonId Numeric(18,0)


AS
	
		IF @Command='persondata'
			BEGIN
				SELECT 
				p.PersonId,
				p.Firstname,
				p.Lastname
					FROM tblperson AS p
						WHERE p.PersonId=@PersonId
			END
		ELSE IF @Command='Hobbydata'
			BEGIN
				SELECT 
				h.Hobbyid,
				h.Hobbyname,
				h.Interested
					FROM tblhobby AS h
						WHERE h.PersonId= @PersonId
			END

