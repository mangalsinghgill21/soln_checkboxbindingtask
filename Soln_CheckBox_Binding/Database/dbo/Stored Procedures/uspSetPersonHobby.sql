﻿CREATE PROCEDURE [dbo].[uspSetPersonHobby]
	@Command Varchar(MAX)=NULL,
	@HobbyId NUmeric(18,0)=NULL,
	@HobbyName varchar(50)=NULL,
	@Intrested bit=NULL,
	@PersonId Numeric(18,0)=NULL,

	@Status int=NULL OUT,
	@Message Varchar(MAX)=NULL OUT

AS
	

	BEGIN
		
		DECLARE @ErrorMessage Varchar(MAX)=NULL

		IF @Command='Insert'
			BEGIN
				
				BEGIN TRANSACTION

				BEGIN TRY
					
	             INSERT INTO tblhobby
					(
					Hobbyname,
					Interested,
					PersonId
					)
			      VALUES
					(
						@HobbyName,
						@Intrested,
						@PersonId
					)

				SET @Status=1
			     SET @Message='Inserted' 

					COMMIT TRANSACTION

				END TRY

				BEGIN CATCH
					
					SET @ErrorMessage=ERROR_MESSAGE()
					SET @Status=0
					SET @Message='not inserted'

					RAISERROR(@ErrorMessage,16,1)
					
					ROLLBACK TRANSACTION

				END CATCH 

			END
			ELSE IF @Command='Update'
			BEGIN 
						
					BEGIN TRANSACTION

					BEGIN TRY 

							UPDATE tblhobby
								SET 
									Hobbyname=@HobbyName,
									Interested=@Intrested,
									PersonId=@PersonId
										where Hobbyid=@HobbyId

								SET @Status=1
								SET @Message='Updated'

								COMMIT TRANSACTION

					END TRY 

					BEGIN CATCH 
						SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION
						
						SET @Status=0
						SET @Message='Update fail'

						RAISERROR(@ErrorMessage,16,1)
					END CATCH


			END 

	End	
