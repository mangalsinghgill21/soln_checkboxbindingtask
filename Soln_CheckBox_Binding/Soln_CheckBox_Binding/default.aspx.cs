﻿using Soln_CheckBox_Binding.Dal;
using Soln_CheckBox_Binding.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Soln_CheckBox_Binding
{
    

    public partial class _default : System.Web.UI.Page
    {
        #region Declaration
        private persondal persondalobj = null;
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #region Private Property
        private String BindLabelMessage
        {
            get
            {
                return lblmessage.Text;
            }
            set
            {
                lblmessage.Text = value;
            }

        }

        private List<hobbyentity> GetHobbyData
        {
            get
            {
                return chkHobby
                    .Items
                    .Cast<ListItem>()
                    .AsEnumerable()
                    .Select((leListCheckBoxObj) => new hobbyentity() {
                        hobbyname = leListCheckBoxObj.Text, 
                        interested = leListCheckBoxObj.Selected 
                    })
                    .ToList();
                                    
            }
        }
        #endregion

        #region Private Methods
        private async Task<personentity> personMappingData()
        {
            return await Task.Run(() =>
            {
                var personentityobj = new personentity()
                {
                    firstname = txtfirstname.Text,
                    lastname =txtlastname.Text,
                    hobby = this.GetHobbyData

                };

                return personentityobj;
            });
        }

        private async Task addpersondata(personentity personentityobj)
        {
            // Create an instance of User Dal.
            persondalobj = new persondal();

            // add Data to Database and Bind to the lable
            this.BindLabelMessage = await persondalobj.Addpersondata(personentityobj);
        }
        #endregion 

        protected async void btnSubmit_Click(object sender, EventArgs e)
        {
               // Mapping User Entity Obj.
            var personentityobj = await this.personMappingData();

            // Add data
            await addpersondata(personentityobj);
        }

    }
}