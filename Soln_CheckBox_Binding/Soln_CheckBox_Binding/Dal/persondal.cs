﻿using Soln_CheckBox_Binding.Dal.ORD;
using Soln_CheckBox_Binding.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Soln_CheckBox_Binding.Dal
{
    public class persondal
    {
       
        #region Declaration
        private personhobbydcDataContext  dc = null;
        #endregion

        #region Constructor
        public persondal()
        {
            dc = new personhobbydcDataContext();
        }
        #endregion

        #region public methods
        public async Task<dynamic> Addpersondata(personentity personentityobj)
        {
            int ? status = null;
            string message = null;
            decimal? getUserId = null;
            try
            {

                return await Task.Run(async () => {
                    var data = 
                             dc
                            .uspSetPerson
                            (
                                    "add",
                                    personentityobj.personid,
                                    personentityobj.firstname,
                                    personentityobj.lastname,
                                    ref status,
                                    ref message,
                                    ref getUserId
                                );

                    
                    personentityobj.personid = getUserId;

                    
                    await this.AddPersonHobbyData(personentityobj);

                    return message;
                });

                
            }
            catch(Exception)
            {
                throw;
            }
        }

        public async Task<personentity> GetPersonData(personentity personentityobj)
        {
            return await Task.Run(() => {

                return

                  dc
                  .uspGetPerson
                  (
                      "persondata",
                      personentityobj.personid
                      )
                  .AsEnumerable()
                  .Select (
                       (leUspGetUsersResultSetObj) => new personentity()
                        {
                            personid = leUspGetUsersResultSetObj.personid,
                            firstname = leUspGetUsersResultSetObj.firstname,
                            lastname = leUspGetUsersResultSetObj.lastname,

                            hobby = (this.GetUserHobbyData(new hobbyentity()
                            {
                                personid = leUspGetUsersResultSetObj.personid
                            })).Result as List<hobbyentity>
                        })

                    .ToList()
                    .FirstOrDefault();



            });
        }

        public async Task<dynamic> Updatepersondata(personentity personentityobj)
        {
            int? status = null;
            string message = null;
            decimal? id = null;

            try
            {
                return await Task.Run(async () =>
                {
                    var setQuery =
                    dc
                    .uspSetPerson
                    (
                        "Update",
                        personentityobj.personid,
                        personentityobj.firstname,
                        personentityobj.lastname,
                        ref status,
                        ref message,
                        ref id
                     );

                    // add Hobby Data 
                    await this.Updatepersonhobbydata(personentityobj);

                    return setQuery;
                });
            }

            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Private Property
        private Func<customgetperson, hobbyentity> SelectpersonHobbyData
        {
            get
            {
                return
                       (leUspGetUsersResultSetObj) => new hobbyentity()
                       {
                           hobbyid = leUspGetUsersResultSetObj.Hobbyid,
                           hobbyname = leUspGetUsersResultSetObj.Hobbyname,
                           interested = leUspGetUsersResultSetObj.Interested
                       };
            }
        }



        #endregion

        #region private methods
        private async Task AddPersonHobbyData(personentity personentityobj)
        {
            int? status = null;
            string message = null;

             await Task.Run(() => {

                foreach(hobbyentity hobbyentityobj in personentityobj.hobby)
                {
                    dc                       
                    .uspSetPersonHobby(
                        "add",
                        hobbyentityobj.hobbyid,
                        hobbyentityobj.hobbyname,
                        hobbyentityobj.interested,
                        personentityobj.personid,
                        ref status,
                        ref message
                        );
                }

               
            });
        }

        private async Task<IEnumerable<hobbyentity>> GetpersonHobbyData(hobbyentity hobbyentityobj)
        {
            return await Task.Run(() => {

                return
                    dc
                    .uspGetPerson(
                        "UserHobbyData",
                        hobbyentityobj.personid
                        )
                    .AsEnumerable()
                    .Select(this.SelectpersonHobbyData)
                    .ToList();

            });
        }
      
        private async Task Updatepersonhobbydata(personentity personentityobj)
        {
            int? status = null;
            string message = null;

            await Task.Run(() =>
            {

                foreach (hobbyentity hobbyobj in personentityobj.hobby)
                {
                    dc
                    .uspSetPersonHobby
                    (
                        "Update",
                        hobbyobj.hobbyid,
                        hobbyobj.hobbyname,
                        hobbyobj.interested,
                        personentityobj.personid,
                        ref status,
                        ref message
                     );
                }
            });
        }

        #endregion
    }
}