﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="getORupdate.aspx.cs" Inherits="Soln_CheckBox_Binding.getORupdate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
            <asp:ScriptManager ID="scriptManager" runat="server">
            </asp:ScriptManager>

            <asp:UpdatePanel ID="updatePanel" runat="server">
                <ContentTemplate>
                    <table>
                <tr>
                    <td>
                            <asp:TextBox ID="txtfirstname" runat="server" PlaceHolder="First Name"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                            <asp:TextBox ID="txtlastname" runat="server" PlaceHolder="Last Name"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                            <asp:CheckBoxList ID="chkHobby" runat="server" RepeatColumns="1" RepeatLayout="Table">
                            </asp:CheckBoxList>
                    </td>
                </tr>
                 <tr>
                    <td>
                            <asp:Button ID="btnupdate" runat="server" Text="Update" OnClick="btnUpdate_Click"/>
                    </td>
                </tr>
                <tr>
                    <td>
                            <asp:Label ID="lblmessage" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
