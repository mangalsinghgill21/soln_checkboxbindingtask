﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Soln_CheckBox_Binding.Entity
{
    public class personentity
    {
        public dynamic personid { get; set; }
        public dynamic firstname { get; set; }
        public dynamic lastname { get; set; }
        public List<hobbyentity> hobby { get; set; }
    }
}