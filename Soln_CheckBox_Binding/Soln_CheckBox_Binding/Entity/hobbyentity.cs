﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Soln_CheckBox_Binding.Entity
{
    public class hobbyentity
    {
        public dynamic hobbyid { get; set; }
        public dynamic hobbyname { get; set; }
        public dynamic interested { get; set; }
        public dynamic personid { get; set; }
    }
}