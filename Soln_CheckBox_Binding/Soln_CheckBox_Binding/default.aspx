﻿<%@ Page Language="C#" AutoEventWireup="true" Async="true" CodeBehind="default.aspx.cs" Inherits="Soln_CheckBox_Binding._default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
            <table>
                <tr>
                    <td>
                            <asp:TextBox ID="txtfirstname" runat="server" PlaceHolder="First Name"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                            <asp:TextBox ID="txtlastname" runat="server" PlaceHolder="Last Name"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                            <asp:CheckBoxList ID="chkHobby" runat="server" RepeatColumns="1" RepeatLayout="Table">
                                <asp:ListItem Text="Playing Cricket" Selected="False"></asp:ListItem>
                                <asp:ListItem Text="Book Reading" Selected="False"></asp:ListItem>
                                <asp:ListItem Text="Dancing" Selected="False"></asp:ListItem>
                                <asp:ListItem Text="Cooking" Selected="False"></asp:ListItem>
                                <asp:ListItem Text="Drawing" Selected="False"></asp:ListItem>
                            </asp:CheckBoxList>
                    </td>
                </tr>
                 <tr>
                    <td>
                            <asp:Button ID="btnsubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                            <asp:Label ID="lblmessage" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
    </div>
    </form>
</body>
</html>
