﻿using Soln_CheckBox_Binding.Dal;
using Soln_CheckBox_Binding.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Soln_CheckBox_Binding
{
    public partial class getORupdate : System.Web.UI.Page
    {
        #region Declaration
        private persondal persondalobj = null;
        #endregion

        protected async void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
                await BindUserData();
        }

        #region Private Property
        private String FirstNameBinding
        {
            get
            {
                return txtfirstname.Text;
            }
            set
            {
                txtfirstname.Text = value;
            }
        }

        private String LastNameBinding
        {
            get
            {
                return txtlastname.Text;
            }
            set
            {
                txtlastname.Text = value;
            }
        }
        #endregion

        #region hobby binding
        private List<hobbyentity> HobbyBinding
        {
            get
            {

                return
                     chkHobby
                     .Items
                     .Cast<ListItem>()
                     .AsEnumerable()
                     .Select((leListItemobj) => new hobbyentity()
                     {
                         hobbyid = Convert.ToDecimal(leListItemobj.Value),
                         hobbyname = leListItemobj.Text,
                         interested = leListItemobj.Selected
                     })
                     .ToList();

            }
            set
            {
                chkHobby.DataSource = value;
                chkHobby.DataBind();

               foreach (ListItem listobj in chkHobby.Items)
                {
                    listobj.Selected = Convert.ToBoolean(
                            value
                            .AsEnumerable()
                            .Where((leUserentityObj) => leUserentityObj.hobbyname == listobj.Text)
                            .FirstOrDefault()
                            .interested);
                }
            }
        }
        #endregion


        #region
        private async Task BindUserData()
        {
           
            persondalobj = new persondal();

            
            var getpersondata = await persondalobj.GetPersonData(new personentity()
            {
                personid = 1
            });

           
            await this.UserMappingEntityToUiData(getpersondata);
        }

        private async Task UserMappingEntityToUiData(personentity personentityobj)
        {
            await Task.Run(() =>
            {

                // Mapping
                this.FirstNameBinding = personentityobj.firstname;
                this.LastNameBinding = personentityobj.lastname;

                this.HobbyBinding = personentityobj.hobby; 
            });
        }

      
        private async Task<personentity> UserMappingUiToEntityData()
        {
            return await Task.Run(() =>
            {
                var personentityobj = new personentity()
                {
                    personid = 1,
                    firstname = FirstNameBinding,
                    lastname = LastNameBinding,

                    hobby = HobbyBinding
                };

                return personentityobj;
            });
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected async void btnUpdate_Click(object sender, EventArgs e)
        {
            var personentityobj = await this.UserMappingUiToEntityData();

            await Updatepersondata(personentityobj);
        }
    }
}